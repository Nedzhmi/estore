package com.example.eStore.api;

import com.example.eStore.model.ApiResponse;
import com.example.eStore.model.Tweet;
import com.example.eStore.service.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.TwitterException;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("api/v1/tweet")
@RestController
public class TwitterController {

    @Autowired
    private TwitterService twitterService;

    @PostMapping
    public ApiResponse tweet(@RequestBody Tweet tweet) {

        try {
            twitterService.createTweet(tweet.getText());
            return new ApiResponse(true, "Successfully tweeted");
        } catch (TwitterException e) {
            return new ApiResponse(false, "Twitter error has occurred: " +
                    e.getErrorMessage() + " Error code: " + e.getErrorCode());
        }


    }
}
