package com.example.eStore.controller;

import com.example.eStore.model.Rank;
import com.example.eStore.service.ClientService;
import com.example.eStore.service.UtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("client")
@Controller
public class ClientViewController extends BaseViewController {
    @Autowired
    private ClientService clientService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model) {
        String role = utilityService.getCurrentUserRole();

        if(Rank.ADMINISTRATOR.toString().equals(role)) {
            model.addAttribute("clients", this.clientService.getAll());
        } else {
            model.addAttribute("clients", this.clientService.getAll(utilityService.getCurrentUserId()));
        }

        model.addAttribute("editLink", "/client/edit/");
        loadHeaderData(model);

        return "client/list";
    }

    @RequestMapping("/add")
    public String add(HttpServletRequest request, Model model) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("client", null);
        model.addAttribute("action", "Add");
        loadHeaderData(model);

        return "client/add";
    }

    @RequestMapping("/edit/{id}")
    public String edit(HttpServletRequest request, Model model, @PathVariable("id") String id) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("client", this.clientService.getClientBy(Integer.parseInt(id)).orElse(null));
        model.addAttribute("action", "Edit");
        loadHeaderData(model);

        return "client/add";
    }
}