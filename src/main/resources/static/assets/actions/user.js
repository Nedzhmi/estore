let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");

let UserActions = {
    addUser: function() {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/user",
            type : "POST",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "firstName": $("#first_name").val(),
                "lastName": $("#last_name").val(),
                "username": $("#username").val(),
                "password": $("#password").val(),
                "rank": $("#role").val().toLowerCase(),
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "User added successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't add user!", "error");
            }
        });
    },

    editUser: function(id) {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/user/" + id,
            type : "PUT",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "firstName": $("#first_name").val(),
                "lastName": $("#last_name").val(),
                "username": $("#username").val(),
                "password": $("#password").val(),
                "rank": $("#role").val().toLowerCase(),
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "User updated successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't update user!", "error");
            }
        });
    },

    deleteUser: function (id) {

        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this user!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/api/v1/user/" + id,
                        type : "DELETE",
                        dataType : 'json',
                        contentType : "application/json; charset=UTF-8",
                        beforeSend: function( xhr ) {
                            xhr.setRequestHeader(header, token);
                        },
                        data: JSON.stringify({
                        }),
                        success: function(data, status) {
                            location.reload();
                        },
                        error: function(xhr, status, error) {
                            location.reload();
                        }
                    });
                    swal("Deleted!", "Your user has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your user is safe :)", "error");
                }
            });
    }
};

$(document).ready(function(){
    /** prevent add user form to be submitted */
    $("#user_form").submit(function(e){
        return false;
    });

    $("#submit_button").click(function(e) {
        if($.trim($('#first_name').val()) === ''){
            swal("Error!", "Please fill in the first name input!", "error");
        } else if ($.trim($('#last_name').val()) === '') {
            swal("Error!", "Please fill in the last name input!", "error");
        } else if ($.trim($('#username').val()) === '') {
            swal("Error!", "Please fill in the username input!", "error");
        } else if ($.trim($('#password').val()) === '') {
            swal("Error!", "Please fill in the password input!", "error");
        } else {
            if($(this).data("action") === "Add") {
                UserActions.addUser();
                $("#user_form")[0].reset();
            } else {
                let id = $(this).data("id");
                UserActions.editUser(id);
            }
        }
    });

    $("button.delete").click(function (e) {
        let id = $(this).data("id");
        UserActions.deleteUser(id);
    });

});