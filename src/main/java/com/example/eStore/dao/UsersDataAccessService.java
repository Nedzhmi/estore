package com.example.eStore.dao;

import com.example.eStore.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface UsersDataAccessService extends JpaRepository<Users, Integer> {

    @Query("FROM Users u WHERE u.username = :username")
    Users findByUsername(@Param("username") String username);

    @Modifying
    @Transactional
    @Query("UPDATE Users u SET u.deleted = 1 WHERE u.id = :id")
    void softDeleteById(@Param("id") int id);

}
