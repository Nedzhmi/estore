package com.example.eStore.service;

import com.example.eStore.dao.SalesDataAccessService;
import com.example.eStore.model.BaseEntity;
import com.example.eStore.model.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class SaleService {
    private final SalesDataAccessService salesDataAccessService;

    @Autowired
    public SaleService(SalesDataAccessService salesDataAccessService) {
        this.salesDataAccessService = salesDataAccessService;
    }

    public Sales addSale(Sales sale) {
        return this.salesDataAccessService.save(sale);
    }

    public List<Sales> getAll() {
        return this.salesDataAccessService.findAll();
    }

    public List<Sales> getAll(int id) {
        return this.salesDataAccessService.findAllByUserId(id);
    }

    public Optional<Sales> getSaleBy(int id) {
        return salesDataAccessService.findById(id);
    }

    public void delete(int id) {
        salesDataAccessService.deleteById(id);
    }

    public void softDeleteById(int id) {
        salesDataAccessService.softDeleteById(id);
    }

    public int update(int id, Sales newSale) {
        Optional<Sales> sale = salesDataAccessService.findById(id);
        if (sale.isPresent()) {
            newSale.setId(id);
            salesDataAccessService.save(newSale);
            return 1;
        }
        return 0;
    }

    public List<Sales> getSalesByStartDate(Date startDate) {
        return this.salesDataAccessService.getSalesByStartDate(startDate);
    }


    public List<Sales> getSalesByEndDate(Date endDate) {
        return this.salesDataAccessService.getSalesByEndDate(endDate);
    }

    public List<Sales> findAllByUserId(Integer userId) {
        return this.salesDataAccessService.findAllByUserId(userId);
    }

    public List<Sales> getSalesByTimeRange(Date startDate, Date endDate) {
        return this.salesDataAccessService.getSalesByTimeRange(startDate, endDate);
    }


    public List<Sales> getSalesByStartDateAndUserId(Date startDate, Integer userId) {
        return this.salesDataAccessService.getSalesByStartDateAndUserId(startDate, userId);
    }


    public List<Sales> getSalesByEndDateAndUserId(Date endDate, Integer userId) {
        return this.salesDataAccessService.getSalesByEndDateAndUserId(endDate, userId);
    }


    public List<Sales> getSalesByTimeRangeAndUserId(Date startDate, Date endDate, Integer userId) {
        return this.salesDataAccessService.getSalesByTimeRangeAndUserId(startDate, endDate, userId);
    }

    public Double getTotalAmount() {
        return this.salesDataAccessService.getTotalAmount();
    }
}
