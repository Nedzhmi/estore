package com.example.eStore.dao;

import com.example.eStore.model.Sales;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
public interface SalesDataAccessService extends JpaRepository<Sales, Integer> {

    @Query("SELECT SUM(s.quantitySold * p.price) FROM Sales s INNER JOIN Products p ON s.product.id = p.id")
    Double getTotalAmount();

    @Query("FROM Sales s WHERE s.date >= :startDate")
    List<Sales> getSalesByStartDate(@Param("startDate") Date startDate);

    @Query("FROM Sales s WHERE s.date <= :endDate")
    List<Sales> getSalesByEndDate(@Param("endDate") Date endDate);

    @Query("FROM Sales s WHERE s.soldBy.id = :userId")
    List<Sales> findAllByUserId(@Param("userId") Integer id);

    @Query("FROM Sales s WHERE s.date BETWEEN :startDate AND :endDate")
    List<Sales> getSalesByTimeRange(@Param("startDate") Date startDate, @Param("endDate") Date endDate);

    @Query("FROM Sales s WHERE s.date >= :startDate AND s.soldBy.id = :userId")
    List<Sales> getSalesByStartDateAndUserId(@Param("startDate") Date startDate,
                                             @Param("userId") Integer userId);

    @Query("FROM Sales s WHERE s.date <= :endDate AND s.soldBy.id = :userId")
    List<Sales> getSalesByEndDateAndUserId(@Param("endDate") Date endDate, @Param("userId") Integer userId);

    @Query("FROM Sales s WHERE s.date BETWEEN :startDate AND :endDate" +
            " AND s.soldBy.id = :userId")
    List<Sales> getSalesByTimeRangeAndUserId(@Param("startDate") Date startDate,
                                             @Param("endDate") Date endDate,
                                             @Param("userId") Integer userId);

    @Modifying
    @Transactional
    @Query("UPDATE Sales s SET s.deleted = 1 WHERE s.id = :id")
    void softDeleteById(@Param("id") int id);
}
