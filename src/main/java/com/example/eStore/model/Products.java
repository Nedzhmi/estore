package com.example.eStore.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "products")
@Where(clause="deleted=0")
public class Products extends BaseEntity implements Serializable {

    public static final long serialVersionUID = 1L;

    private String name;
    private int quantity;
    private int criticalQuantity;
    private double price;
    private Users user;

    public Products() {
    }

    public Products(String name, int quantity, int criticalQuantity, double price, Users user) {
        this.name = name;
        this.quantity = quantity;
        this.criticalQuantity = criticalQuantity;
        this.price = price;
        this.user = user;
    }

    @Column(name = "name")
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "quantity")
    @NotNull
    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Column(name = "critical_quantity")
    @NotNull
    public int getCriticalQuantity() {
        return criticalQuantity;
    }

    public void setCriticalQuantity(int criticalQuantity) {
        this.criticalQuantity = criticalQuantity;
    }

    @Column(name = "price")
    @NotNull
    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
