package com.example.eStore.dao;

import com.example.eStore.model.Clients;
import com.example.eStore.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ClientsDataAccessService extends JpaRepository<Clients, Integer> {

    @Query("FROM Clients c WHERE c.user.id = :userId")
    List<Clients> findAllByUserId(@Param("userId") Integer id);

    @Modifying
    @Transactional
    @Query("UPDATE Clients c SET c.deleted = 1 WHERE c.id = :id")
    void softDeleteById(@Param("id") int id);
}
