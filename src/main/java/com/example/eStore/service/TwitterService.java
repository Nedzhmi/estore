package com.example.eStore.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

@Service
public class TwitterService {

    private Twitter twitter;


    public TwitterService() {
        TwitterFactory tf = new TwitterFactory();
        this.twitter = tf.getInstance();
    }

    public Status createTweet(String tweet) throws TwitterException {
        //        return status.getText();
        return twitter.updateStatus(tweet);
    }

}
