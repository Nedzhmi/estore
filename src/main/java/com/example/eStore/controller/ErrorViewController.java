package com.example.eStore.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ErrorViewController  implements ErrorController {

    private static final String PATH = "errors/error";

    @RequestMapping("/error")
    public String handleError() {
        return "errors/error";
    }

    @Override
    public String getErrorPath() {
        return null;
    }

}
