let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");


let SaleActions = {
    let: currProductId = document.getElementById("product"),
    let: currSoldToId = document.getElementById("buyer"),

    addSale: function () {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/sale",
            type : "POST",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function ( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify( {
                "product": $(currProductId).val(),
                "quantitySold": $("#quantity_sold").val(),
                "soldTo": $(currSoldToId).val()
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                if(data.success){
                    swal("Successful!", "Sale added successfully!", "success");
                    $("#sale_form")[0].reset();
                } else {
                    swal("Error!", data.message, "error");
                }
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't add sale!", "error");
            }
        })
    },

    editSale: function(id) {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/sale/" + id,
            type : "PUT",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "product": $(currProductId).val(),
                "quantitySold": $("#quantity_sold").val(),
                "soldTo": $(currSoldToId).val()
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "Sale edited successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't edit sale!", "error");
            }
        });
    },

    deleteSale: function (id) {

        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this sale!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/api/v1/sale/" + id,
                        type : "DELETE",
                        dataType : 'json',
                        contentType : "application/json; charset=UTF-8",
                        beforeSend: function( xhr ) {
                            xhr.setRequestHeader(header, token);
                        },
                        data: JSON.stringify({
                        }),
                        success: function(data, status) {
                            location.reload();
                        },
                        error: function(xhr, status, error) {
                            location.reload();
                        }
                    });
                    swal("Deleted!", "Your sale has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your sale is safe :)", "error");
                }
            });
    }
};


$(document).ready(function(){
    /** prevent add product form to be submitted */
    $("#product_form").submit(function(e){
        return false;
    });

    $("#submit_button").click(function(e) {
        if($.trim($('#quantity_sold').val()) === '' || $('#quantity_sold').val() <= 0){
            swal("Error!", "Please fill in the quantity sold input!", "error");
        } else {
            if($(this).data("action") === "Add") {
                SaleActions.addSale();
            } else {
                 let id = $(this).data("id");
                 SaleActions.editSale(id);
            }
        }
    });

    $("button.delete").click(function (e){
        let id = $(this).data("id");
        SaleActions.deleteSale(id);
    });
});