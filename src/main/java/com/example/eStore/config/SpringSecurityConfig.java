package com.example.eStore.config;

import com.example.eStore.model.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AuthenticationEntryPoint authEntryPoint;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().and()
            .cors().disable()
            /* Access to Auth Pages */
            .authorizeRequests()
                .antMatchers("/api/v1/client/**").hasAnyAuthority(Rank.ADMINISTRATOR.toString(), Rank.MERCHANT.toString())
                .antMatchers("/api/v1/product/**").hasAuthority(Rank.ADMINISTRATOR.toString())
                .antMatchers("/api/v1/sale/**").hasAnyAuthority(Rank.ADMINISTRATOR.toString(), Rank.MERCHANT.toString())
                .antMatchers("/api/v1/user/**").hasAuthority(Rank.ADMINISTRATOR.toString())
                .antMatchers("/api/v1/tweet/**").hasAuthority(Rank.ADMINISTRATOR.toString())

                /* Manager */
                .antMatchers("/dashboard/**").hasAnyAuthority(Rank.ADMINISTRATOR.toString(), Rank.MERCHANT.toString())
                .antMatchers("/client/**").hasAnyAuthority(Rank.ADMINISTRATOR.toString(), Rank.MERCHANT.toString())
                .antMatchers("/product/**").hasAuthority(Rank.ADMINISTRATOR.toString())
                .antMatchers("/sale/**").hasAnyAuthority(Rank.ADMINISTRATOR.toString(), Rank.MERCHANT.toString())
                .antMatchers("/user/**").hasAuthority(Rank.ADMINISTRATOR.toString())
                .antMatchers("/login", "/assets/**", "/403").permitAll()
                .anyRequest().authenticated()
            .and()
                .httpBasic()
                .authenticationEntryPoint(authEntryPoint)

            /* Login & Logout pages */
            .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("username").passwordParameter("password")
                .loginProcessingUrl("/doLogin")
                .defaultSuccessUrl("/login_success", true)
                .successForwardUrl("/dashboard/index")
                .failureUrl("/login?error=true")

            .and()
                .logout()
                .logoutSuccessUrl("/login").permitAll()
            .and()
                .exceptionHandling().accessDeniedPage("/403");

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder)
                .usersByUsernameQuery("SELECT username, password, 1 FROM `users` WHERE username = ?")
                .authoritiesByUsernameQuery("SELECT u.username, u.rank FROM users u WHERE u.username = ?");
    }
}