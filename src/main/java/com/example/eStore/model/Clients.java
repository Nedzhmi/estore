package com.example.eStore.model;

import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "clients")
@Where(clause="deleted=0")
public class Clients extends BaseEntity implements Serializable {

    public static final long serialVersionUID = 2L;

    private String name;
    private String lastName;
    private Users user;

    public Clients(String name, String lastName, Users user) {
        this.name = name;
        this.lastName = lastName;
        this.user = user;
    }

    public Clients() {
    }

    @Column(name = "name")
    @NotNull
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "last_name")
    @NotNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @ManyToOne
    @JoinColumn(name = "merchant_id")
    @NotNull
    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String buildFullName() {
        return  this.name + " " + this.lastName;
    }

}
