package com.example.eStore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Rank {
    @JsonProperty("merchant")
    MERCHANT,
    @JsonProperty("administrator")
    ADMINISTRATOR;
}
