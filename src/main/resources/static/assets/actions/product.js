let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");


let ProductActions = {
    addProduct: function() {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/product",
            type : "POST",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "name": $("#name").val(),
                "price": $("#price").val(),
                "quantity": $("#quantity").val(),
                "criticalQuantity": $("#critical_quantity").val(),
                "user": {
                    "id": $("#userId").val()
                },
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "Product added successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't add product!", "error");
            }
        });
    },

    editProduct: function(id) {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/product/" + id,
            type : "PUT",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "name": $("#name").val(),
                "price": $("#price").val(),
                "quantity": $("#quantity").val(),
                "criticalQuantity": $("#critical_quantity").val(),
                "user": {
                    "id": $("#userId").val()
                },
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "Product edited successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't edit product!", "error");
            }
        });
    },

    deleteProduct: function (id) {

        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/api/v1/product/" + id,
                        type : "DELETE",
                        dataType : 'json',
                        contentType : "application/json; charset=UTF-8",
                        beforeSend: function( xhr ) {
                            xhr.setRequestHeader(header, token);
                        },
                        data: JSON.stringify({
                        }),
                        success: function(data, status) {
                            location.reload();
                        },
                        error: function(xhr, status, error) {
                            location.reload();
                        }
                    });
                    swal("Deleted!", "Your product has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your product is safe :)", "error");
                }
            });
    }
};

$(document).ready(function(){
    /** prevent add product form to be submitted */
    $("#product_form").submit(function(e){
        return false;
    });

    $("#submit_button").click(function(e) {
        if($.trim($('#name').val()) === ''){
            swal("Error!", "Please fill in the name input!", "error");
        } else if ($.trim($('#price').val()) === '' || $('#price').val() <= 0) {
            swal("Error!", "Please fill in the price input!", "error");
        }else if ($.trim($('#quantity').val()) === '' || $('#quantity').val() <= 0) {
            swal("Error!", "Please fill in the quantity input!", "error");
        } else if ($.trim($('#critical_quantity').val()) === '' || $('#critical_quantity').val() <= 0) {
            swal("Error!", "Please fill in the critical quantity input!", "error");
        } else {
            if($(this).data("action") === "Add") {
                ProductActions.addProduct();
                $("#product_form")[0].reset();
            } else {
                let id = $(this).data("id");
                ProductActions.editProduct(id);
            }
        }
    });

    $("button.delete").click(function (e){
        let id = $(this).data("id");
        ProductActions.deleteProduct(id);
    });
});