package com.example.eStore.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class LoginController {
    @RequestMapping(value = {"/", "/login"})
    public String index(HttpServletRequest request, Model model){
        String error = request.getParameter("error");
        if (error != null && !error.isEmpty() && error.equals("true")) {
            model.addAttribute("showError", "1");
        }

        return "login/index";
    }

    @RequestMapping("/doLogin")
    public RedirectView redirectToUser(RedirectAttributes attributes) {
        attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
        attributes.addAttribute("attribute", "redirectWithRedirectView");

        return new RedirectView("/dashboard/index");
    }

    @PostMapping("/login_success_handler")
    public String loginSuccessHandler() {
        return "login/index";
    }

    @RequestMapping("/403")
    public String notFound() {
        return "errors/permission-denied";
    }

    @RequestMapping("/logout")
    public RedirectView logout(RedirectAttributes attributes, HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        return new RedirectView("/dashboard/index");
    }
}