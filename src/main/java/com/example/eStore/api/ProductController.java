package com.example.eStore.api;

import com.example.eStore.model.ApiResponse;
import com.example.eStore.model.Products;
import com.example.eStore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/product")
@RestController
public class ProductController {
    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping
    public ApiResponse addProduct(@Validated @RequestBody Products product) {
        if (productService.addProduct(product) != null) {
            return new ApiResponse(true, "Successfully inserted");
        }

        return new ApiResponse(false, "Something wrong, please try again");
    }

    @GetMapping
    public List<Products> getProducts() {
        return productService.getAll();
    }

    @GetMapping(path = "/{id}")
    public Products productsBy(@PathVariable("id")String id) {
        return productService.getProductBy(Integer.parseInt(id)).orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public ApiResponse delete(@PathVariable("id") String id) {
        productService.softDeleteById(Integer.parseInt(id));
        return new ApiResponse(true, "");
    }

    @PutMapping(path = "/{id}")
    public ApiResponse update(@PathVariable("id") String id, @Validated @RequestBody Products product) {
        productService.update(Integer.parseInt(id), product);
        return new ApiResponse(true, "");
    }
}
