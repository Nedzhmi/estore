let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");

let TweetActions = {
    addTweet: function () {

        $.ajax({
            url: "/api/v1/tweet",
            type: "POST",
            dataType: 'json',
            contentType: "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "text": $("#tweet_text").val()
            }),
            success: function(data, status) {
                swal("Successful!", "Tweeted successfully!", "success");
            },
            error: function(xhr, status, error) {
                swal("Error!", "Couldn't tweet!", "error");
            }
        });
    }
}


$(document).ready(function(){

    $("#tweet_button").click(function(e) {
        if($.trim($('#tweet_text').val()) === ''){
            swal("Error!", "Please fill in the tweet text!", "error");
        } else {
            TweetActions.addTweet();
        }
    });
});