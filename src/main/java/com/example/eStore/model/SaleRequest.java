package com.example.eStore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class SaleRequest implements Serializable {

    public static final long serialVersionUID = 5L;

    @NotNull
//    @Min(value = 1, message = "Must be greater or equal to 1.")
    @JsonProperty("product")
    private Integer productId;
    @NotNull
//    @Min(value = 1, message = "Must be greater or equal to 1.")
    @JsonProperty("quantitySold")
    private Integer quantitySold;
    @NotNull
//    @Min(value = 1, message = "Must be greater or equal to 1.")
    @JsonProperty("soldTo")
    private Integer soldTo;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(Integer quantitySold) {
        this.quantitySold = quantitySold;
    }

    public Integer getSoldTo() {
        return soldTo;
    }

    public void setSoldTo(Integer soldTo) {
        this.soldTo = soldTo;
    }
}
