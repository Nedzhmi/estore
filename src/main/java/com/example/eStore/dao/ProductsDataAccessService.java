package com.example.eStore.dao;

import com.example.eStore.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ProductsDataAccessService extends JpaRepository<Products, Integer> {

    @Query("FROM Products p WHERE p.user.id = :userId")
    List<Products> findAllByUserId(@Param("userId") Integer id);

    @Modifying
    @Transactional
    @Query("UPDATE Products p SET p.deleted = 1 WHERE p.id = :id")
    void softDeleteById(@Param("id") int id);
}
