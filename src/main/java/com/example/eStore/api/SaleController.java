package com.example.eStore.api;

import com.example.eStore.dao.ProductsDataAccessService;
import com.example.eStore.model.*;
import com.example.eStore.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.List;

@RequestMapping("api/v1/sale")
@RestController
public class SaleController {
    private final SaleService saleService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private UtilityService utilityService;
    @Autowired
    private ProductsDataAccessService productsDataAccessService;
    @Autowired
    private EmailService emailService;

    @Autowired
    public SaleController(SaleService saleService) {
        this.saleService = saleService;
    }

    @PostMapping
    public ApiResponse addSale(@Validated @RequestBody SaleRequest saleRequest) {

        Products product = productService.getProductBy(saleRequest.getProductId()).orElse(null);
        if(product == null) {
            return new ApiResponse(false, "Product doesn't exist, please try again");
        }

        Integer quantitySold = saleRequest.getQuantitySold();
        Clients client = clientService.getClientBy(saleRequest.getSoldTo()).orElse(null);
        if(client == null) {
            return new ApiResponse(false, "Client doesn't exist, please try again");
        }

        Users user = utilityService.getCurrentUser();
        java.util.Date currDate = new java.util.Date();

        //check if the quantity in the store is enough to complete the sale
        if (quantitySold > product.getQuantity()) {
            return new ApiResponse(false, "Cannot complete the sale, not enough quantity");
        }

        Sales sale = new Sales();
        sale.setProduct(product);
        sale.setQuantitySold(quantitySold);
        sale.setSoldBy(user);
        sale.setSoldTo(client);
        sale.setDate(new Date(currDate.getTime()));
        sale.setDeleted(0);

        //decrease the quantity of a product in the store after a sale is made
        int resultQuantity = product.getQuantity() - quantitySold;
        product.setQuantity(resultQuantity);
        productsDataAccessService.save(product);

        if(resultQuantity <= product.getCriticalQuantity()) {
            emailService.sendSimpleMessage("parov13@abv.bg", "Critical quantity reached!",
                    "Warning! You reached critical quantity for " + sale.getProduct().getName());
        }

        if (saleService.addSale(sale) != null) {
            return new ApiResponse(true, "Successfully inserted");
        }

        return new ApiResponse(false, "Something wrong, please try again");
    }

    @GetMapping
    public List<Sales> getSales() {
        return saleService.getAll();
    }

    @GetMapping(path = "/{id}")
    public Sales salesBy(@PathVariable("id") String id) {
        return saleService.getSaleBy(Integer.parseInt(id)).orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public ApiResponse delete(@PathVariable("id") String id) {
        Sales sale = saleService.getSaleBy(Integer.parseInt(id)).orElse(null);

        if(sale != null) {
            Products product = sale.getProduct();

            //increase the quantity of a product when a sale is deleted
            int resultQuantity = product.getQuantity() + sale.getQuantitySold();
            product.setQuantity(resultQuantity);
            productsDataAccessService.save(product);
            saleService.softDeleteById(Integer.parseInt(id));
            return new ApiResponse(true, "");
        } else {
            return new ApiResponse(false, "Something wrong, please try again");
        }
    }

    @PutMapping(path = "/{id}")
    public ApiResponse update(@PathVariable("id") String id, @Validated @RequestBody Sales sale) {
        saleService.update(Integer.parseInt(id), sale);
        return new ApiResponse(true, "");
    }

}
