package com.example.eStore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@MappedSuperclass
public abstract class BaseEntity {

    private Integer id;
    private Integer deleted;

    public BaseEntity() {
    }

    public BaseEntity(String id) {
        this.id = Integer.parseInt(id);
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = Integer.parseInt(id);
    }

    @Column(name = "deleted", columnDefinition = "int default 0")
    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }
}
