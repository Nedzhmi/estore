package com.example.eStore.service;

import com.example.eStore.dao.ProductsDataAccessService;
import com.example.eStore.model.BaseEntity;
import com.example.eStore.model.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductsDataAccessService productsDataAccessService;

    @Autowired
    public ProductService(ProductsDataAccessService productsDataAccessService) {
        this.productsDataAccessService = productsDataAccessService;
    }

    public Products addProduct(Products product) {
        return this.productsDataAccessService.save(product);
    }

    public List<Products>  getAll() {
        return productsDataAccessService.findAll();
    }

    public List<Products> getAll(Integer userId) {
        return productsDataAccessService.findAllByUserId(userId);
    }

    public Optional<Products> getProductBy(int id) {
        return productsDataAccessService.findById(id);
    }

    public void delete(int id) {
        productsDataAccessService.deleteById(id);
    }

    public void softDeleteById(int id) {
        productsDataAccessService.softDeleteById(id);
    }

    public int update (int id, Products newProduct) {
        Optional<Products> product = productsDataAccessService.findById(id);
        if (product.isPresent()) {
            newProduct.setId(id);
            productsDataAccessService.save(newProduct);
            return 1;
        }
        return 0;
    }
}
