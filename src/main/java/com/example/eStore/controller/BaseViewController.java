package com.example.eStore.controller;

import com.example.eStore.service.UtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public class BaseViewController {
    @Autowired
    protected UtilityService utilityService;

    protected void loadHeaderData(Model model) {
        model.addAttribute("username", this.utilityService.getCurrentUsername());

        model.addAttribute("role", this.utilityService.getCurrentUserRole());
    }
}
