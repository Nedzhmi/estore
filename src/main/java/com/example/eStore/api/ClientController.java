package com.example.eStore.api;

import com.example.eStore.model.ApiResponse;
import com.example.eStore.model.Clients;
import com.example.eStore.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("api/v1/client")
@RestController
public class ClientController {
    private final ClientService clientService;

    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ApiResponse addClient(@Validated @RequestBody Clients client) {
        if (clientService.addClient(client) != null) {
            return new ApiResponse(true, "Successfully inserted");
        }

        return new ApiResponse(false, "Something wrong, please try again");
    }

    @GetMapping
    public List<Clients> getClients() {
        return clientService.getAll();
    }

    @GetMapping(path = "/{id}")
    public Clients clientsBy(@PathVariable("id") String id) {
        return clientService.getClientBy(Integer.parseInt(id)).orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public ApiResponse delete(@PathVariable("id") String id) {
        clientService.softDeleteById(Integer.parseInt(id));
        return new ApiResponse(true, "");
    }

    @PutMapping(path = "/{id}")
    public ApiResponse update(@PathVariable("id") String id, @Validated @RequestBody Clients client) {
        clientService.update(Integer.parseInt(id), client);
        return new ApiResponse(true, "");
    }
}
