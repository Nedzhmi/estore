package com.example.eStore.service;

import com.example.eStore.dao.UsersDataAccessService;
import com.example.eStore.model.Rank;
import com.example.eStore.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    private final UsersDataAccessService usersDataAccessService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UserService(UsersDataAccessService usersDataAccessService) {
        this.usersDataAccessService = usersDataAccessService;
    }

    public Users addUser(Users user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return this.usersDataAccessService.save(user);
    }

    public List<Users> getAll() {
        return usersDataAccessService.findAll();
    }

    public Optional<Users> getUserBy(int id) {
        return usersDataAccessService.findById(id);
    }

    public void delete(int id) {
        usersDataAccessService.deleteById(id);
    }

    public void softDeleteById(int id) {
        usersDataAccessService.softDeleteById(id);
    }

    public int update(int id, Users newUser) {
        Optional<Users> user = usersDataAccessService.findById(id);
        if (user.isPresent()) {
            newUser.setId(id);
            newUser.setPassword(passwordEncoder.encode(newUser.getPassword()));
            usersDataAccessService.save(newUser);
            return 1;
        }
        return 0;
    }
}
