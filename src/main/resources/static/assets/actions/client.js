let token = $("meta[name='_csrf']").attr("content");
let header = $("meta[name='_csrf_header']").attr("content");

let ClientActions = {
    addClient: function() {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/client",
            type : "POST",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "name": $("#name").val(),
                "lastName": $("#last_name").val(),
                "user": {
                    "id": $("#userId").val()
                },
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "Client added successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't add client!", "error");
            }
        });
    },

    editClient: function(id) {
        $('#submit_button').attr('disabled', 'disabled');

        $.ajax({
            url: "/api/v1/client/" + id,
            type : "PUT",
            dataType : 'json',
            contentType : "application/json; charset=UTF-8",
            beforeSend: function( xhr ) {
                xhr.setRequestHeader(header, token);
            },
            data: JSON.stringify({
                "name": $("#name").val(),
                "lastName": $("#last_name").val(),
                "user": {
                    "id": $("#userId").val()
                },
                "deleted": 0
            }),
            success: function(data, status) {
                $('#submit_button').removeAttr('disabled');
                swal("Successful!", "Client updated successfully!", "success");
            },
            error: function(xhr, status, error) {
                $('#submit_button').removeAttr('disabled');
                swal("Error!", "Couldn't updated client!", "error");
            }
        });
    },

    deleteClient: function (id) {

        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this client!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Yes, delete it!",
                cancelButtonText: "No, cancel please!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "/api/v1/client/" + id,
                        type : "DELETE",
                        dataType : 'json',
                        contentType : "application/json; charset=UTF-8",
                        beforeSend: function( xhr ) {
                            xhr.setRequestHeader(header, token);
                        },
                        data: JSON.stringify({
                        }),
                        success: function(data, status) {
                            location.reload();
                        },
                        error: function(xhr, status, error) {
                            location.reload();
                        }
                    });
                    swal("Deleted!", "Your client has been deleted.", "success");
                } else {
                    swal("Cancelled", "Your client is safe :)", "error");
                }
            });
    }
};

$(document).ready(function(){
    /** prevent add client form to be submitted */
    $("#client_form").submit(function(e){
        return false;
    });

    $("#submit_button").click(function(e) {
        if($.trim($('#name').val()) === ''){
            swal("Error!", "Please fill in the name input!", "error");
        } else if ($.trim($('#last_name').val()) === '') {
            swal("Error!", "Please fill in the last name input!", "error");
        } else {
            if($(this).data("action") === "Add") {
                ClientActions.addClient();
                $("#client_form")[0].reset();
            } else {
                let id = $(this).data("id");
                ClientActions.editClient(id);
            }
        }
    });

    $("button.delete").click(function (e) {
        let id = $(this).data("id");
        ClientActions.deleteClient(id);
    })
});