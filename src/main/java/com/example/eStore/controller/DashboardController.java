package com.example.eStore.controller;

import com.example.eStore.service.ClientService;
import com.example.eStore.service.SaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/dashboard")
@Controller
public class DashboardController extends BaseViewController{

    @Autowired
    private SaleService saleService;
    @Autowired
    private ClientService clientService;

    @RequestMapping(path = {"/", "/index"})
    public String index(HttpServletRequest request, Model model){
        loadHeaderData(model);
        model.addAttribute("salesCount", this.saleService.getAll().size());
        model.addAttribute("totalAmount", this.saleService.getTotalAmount());
        model.addAttribute("clientsCount", this.clientService.getAll().size());

        return "design/index";
    }



}