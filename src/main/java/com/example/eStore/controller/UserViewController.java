package com.example.eStore.controller;

import com.example.eStore.model.Rank;
import com.example.eStore.service.UserService;
import com.example.eStore.service.UtilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("user")
@Controller
public class UserViewController extends BaseViewController{
    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("users", this.userService.getAll());
        model.addAttribute("editLink", "/user/edit/");
        loadHeaderData(model);

        return "user/list";
    }

    @RequestMapping("/add")
    public String add(HttpServletRequest request, Model model) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("user", null);
        model.addAttribute("action", "Add");
        model.addAttribute("roles", Rank.values());
        loadHeaderData(model);

        return "user/add";
    }

    @RequestMapping("/edit/{id}")
    public String edit(HttpServletRequest request, Model model, @PathVariable("id") String id) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("user", this.userService.getUserBy(Integer.parseInt(id)).orElse(null));
        model.addAttribute("action", "Edit");
        model.addAttribute("roles", Rank.values());
        loadHeaderData(model);

        return "user/add";
    }
}
