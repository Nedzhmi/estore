package com.example.eStore.api;

import com.example.eStore.model.ApiResponse;
import com.example.eStore.model.BaseEntity;
import com.example.eStore.model.Rank;
import com.example.eStore.model.Users;
import com.example.eStore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//baseUrl: http://localhost/

/**
 * POST - add, insert - request body / response body
 * PUT - update - request body / response body
 * GET - select - no request body / response body
 * DELETE - delete - no request body / no responce body
 */


@RequestMapping("api/v1/user")
@RestController
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ApiResponse addUser(@Validated @RequestBody Users user) {
        if (userService.addUser(user) != null) {
            return new ApiResponse(true, "Successfully inserted");
        }

        return new ApiResponse(false, "Something wrong, please try again");
    }

    @GetMapping
    public List<Users> getUsers() {
        return userService.getAll();
    }

    @GetMapping(path = "/{id}")
    public Users usersBy(@PathVariable("id") String id){
        return userService.getUserBy(Integer.parseInt(id)).orElse(null);
    }

    @DeleteMapping(path = "/{id}")
    public ApiResponse delete(@PathVariable("id") String id){
        userService.softDeleteById(Integer.parseInt(id));
        return new ApiResponse(true, "");
    }

    @PutMapping(path = "/{id}")
    public ApiResponse update(@PathVariable("id") String id, @Validated @RequestBody Users user){
        userService.update(Integer.parseInt(id) , user);
        return new ApiResponse(true, "");
    }
}
