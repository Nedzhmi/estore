package com.example.eStore.controller;

import com.example.eStore.model.Rank;
import com.example.eStore.model.Sales;
import com.example.eStore.service.ClientService;
import com.example.eStore.service.ProductService;
import com.example.eStore.service.SaleService;
import com.example.eStore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@RequestMapping("sale")
@Controller
public class SaleViewController extends BaseViewController {
    @Autowired
    private SaleService saleService;
    @Autowired
    private ProductService productService;
    @Autowired
    private ClientService clientService;
    @Autowired
    private UserService userService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model,
                       @RequestParam(value = "startDate", required = false) String startDate,
                       @RequestParam(value = "endDate", required = false) String endDate,
                       @RequestParam(value = "seller", required = false) String seller) {

        Integer sellerId = seller != null ? Integer.parseInt(seller) : 0;
        startDate = startDate != null && !startDate.equals("") ? startDate : null;
        endDate = endDate != null && !endDate.equals("") ? endDate : null;
        int currentUserId = this.utilityService.getCurrentUserId();
        String currentUserRole = this.utilityService.getCurrentUserRole();

        if (currentUserRole.equals(Rank.ADMINISTRATOR.toString())) {
            if (startDate == null && endDate == null && sellerId == 0) {
                model.addAttribute("sales", this.saleService.getAll());
            } else {
                if (startDate != null) {
                    java.util.Date startDateObj = null;
                    try {
                        startDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (endDate != null) {
                        java.util.Date endDateObj = null;

                        try {
                            endDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        if (sellerId != 0) {
                            model.addAttribute("sales", this.saleService.getSalesByTimeRangeAndUserId(startDateObj, endDateObj, sellerId));
                        } else {
                            model.addAttribute("sales", this.saleService.getSalesByTimeRange(startDateObj, endDateObj));
                        }
                    } else {
                        if (sellerId != 0) {
                            model.addAttribute("sales", this.saleService.getSalesByStartDateAndUserId(startDateObj, sellerId));
                        } else {
                            model.addAttribute("sales", this.saleService.getSalesByStartDate(startDateObj));
                        }
                    }
                } else {
                    if (endDate != null) {

                        java.util.Date endDateObj = null;

                        try {
                            endDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if (sellerId != 0) {
                            model.addAttribute("sales", this.saleService.getSalesByEndDateAndUserId(endDateObj, sellerId));
                        } else {
                            model.addAttribute("sales", this.saleService.getSalesByEndDate(endDateObj));
                        }
                    } else {
                        model.addAttribute("sales", this.saleService.findAllByUserId(sellerId));
                    }
                }
            }
        } else {
            if (startDate == null && endDate == null) {
                model.addAttribute("sales", this.saleService.findAllByUserId(currentUserId));
            } else {
                if (startDate != null) {
                    java.util.Date startDateObj = null;
                    try {
                        startDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if (endDate != null) {
                        java.util.Date endDateObj = null;

                        try {
                            endDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        model.addAttribute("sales", this.saleService.getSalesByTimeRangeAndUserId(startDateObj, endDateObj, currentUserId));
                    } else {
                        model.addAttribute("sales", this.saleService.getSalesByStartDateAndUserId(startDateObj, currentUserId));
                    }
                } else {
                    java.util.Date endDateObj = null;

                    try {
                        endDateObj = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    model.addAttribute("sales", this.saleService.getSalesByEndDateAndUserId(endDateObj, currentUserId));
                }
            }
        }


        model.addAttribute("userId", currentUserId);
        model.addAttribute("userRole", currentUserRole);
        model.addAttribute("startDate", startDate);
        model.addAttribute("endDate", endDate);
        model.addAttribute("seller", sellerId);
        model.addAttribute("users", this.userService.getAll());
        model.addAttribute("editLink", "/sale/edit/");
        loadHeaderData(model);

        return "sale/list";
    }

    @RequestMapping("/add")
    public String add(HttpServletRequest request, Model model) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("sale", null);
        model.addAttribute("product", null);
        model.addAttribute("client", null);
        model.addAttribute("products", this.productService.getAll());
        model.addAttribute("clients", this.clientService.getAll());
        model.addAttribute("action", "Add");
        loadHeaderData(model);

        return "sale/add";
    }

    @RequestMapping("/edit/{id}")
    public String edit(HttpServletRequest request, Model model, @PathVariable("id") String id) {
        Sales sale = this.saleService.getSaleBy(Integer.parseInt(id)).orElse(null);

        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("sale", sale);

        if (sale != null) {
            model.addAttribute("product", sale.getProduct());
            model.addAttribute("products", this.productService.getAll());
            model.addAttribute("client", sale.getSoldTo());
            model.addAttribute("clients", this.clientService.getAll());
        }

        model.addAttribute("action", "Edit");
        loadHeaderData(model);

        return "sale/add";
    }
}
