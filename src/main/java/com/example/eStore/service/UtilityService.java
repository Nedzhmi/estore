package com.example.eStore.service;

import com.example.eStore.dao.UsersDataAccessService;
import com.example.eStore.model.Rank;
import com.example.eStore.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UtilityService {

    @Autowired
    private UsersDataAccessService usersDataAccessService;

    public String getCurrentUserRole() {
        String role = Rank.ADMINISTRATOR.toString();

        if (SecurityContextHolder.getContext().getAuthentication().getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals(Rank.MERCHANT.toString()))) {
            role = Rank.MERCHANT.toString();
        }

        return role;
    }

    public String getCurrentUsername() {
        User content = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return content.getUsername();
    }

    public Users getCurrentUser() {
        return usersDataAccessService.findByUsername(getCurrentUsername());
    }

    public int getCurrentUserId() {
        return getCurrentUser().getId();
    }
}
