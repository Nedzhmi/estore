package com.example.eStore.model;

import org.hibernate.annotations.Where;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

@Entity
@Table(name = "sales")
@Where(clause="deleted=0")
public class Sales extends BaseEntity implements Serializable {

    public static final long serialVersionUID = 4L;
    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private Products product;
    private int quantitySold;
    private Users soldBy;
    private Clients soldTo;
    @ReadOnlyProperty
    private Date dateSold;

    public Sales(Products product, int quantitySold, Users soldBy, Clients soldTo, Date dateSold) {
        this.product = product;
        this.quantitySold = quantitySold;
        this.soldBy = soldBy;
        this.soldTo = soldTo;
        this.dateSold = dateSold;
    }

    public Sales() {
    }

    @ManyToOne
    @JoinColumn(name = "product_id")
    @NotNull
    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    @Column(name = "quantity_sold")
    @NotNull
    public int getQuantitySold() {
        return quantitySold;
    }

    public void setQuantitySold(int quantitySold) {
        this.quantitySold = quantitySold;
    }

    @ManyToOne
    @JoinColumn(name = "sold_by")
    @NotNull
    public Users getSoldBy() {
        return soldBy;
    }

    public void setSoldBy(Users soldBy) {
        this.soldBy = soldBy;
    }

    @ManyToOne
    @JoinColumn(name = "sold_to")
    @NotNull
    public Clients getSoldTo() {
        return soldTo;
    }

    public void setSoldTo(Clients soldTo) {
        this.soldTo = soldTo;
    }

    @Column(name = "date_sold")
    @NotNull
    public Date getDate() {
        return dateSold;
    }

    public void setDate(Date dateSold) {
        this.dateSold = dateSold;
    }

    private java.sql.Date parseDate(String date) {
        try {
            return new Date(DATE_FORMAT.parse(date).getTime());
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
