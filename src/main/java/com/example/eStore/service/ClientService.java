package com.example.eStore.service;

import com.example.eStore.dao.ClientsDataAccessService;
import com.example.eStore.model.Clients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientService {
    private final ClientsDataAccessService clientsDataAccessService;

    @Autowired
    public ClientService(ClientsDataAccessService clientsDataAccessService) {
        this.clientsDataAccessService = clientsDataAccessService;
    }

    public Clients addClient(Clients client) {
        return this.clientsDataAccessService.save(client);
    }

    public List<Clients> getAll() {
        return clientsDataAccessService.findAll();
    }

    public List<Clients> getAll(Integer userId) {
        return clientsDataAccessService.findAllByUserId(userId);
    }

    public Optional<Clients> getClientBy(int id) {
        return clientsDataAccessService.findById(id);
    }

    public void delete(int id) {
        clientsDataAccessService.deleteById(id);
    }

    public void softDeleteById(int id) {
        clientsDataAccessService.softDeleteById(id);
    }

    public int update(int id, Clients newClient) {
        Optional<Clients> client = clientsDataAccessService.findById(id);
        if (client.isPresent()) {
            newClient.setId(id);
            clientsDataAccessService.save(newClient);
            return 1;
        }
        return 0;
    }
}
