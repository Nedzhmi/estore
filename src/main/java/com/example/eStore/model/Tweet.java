package com.example.eStore.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Tweet {
    @JsonProperty("text")
    private String text;

    public Tweet(String text) {
        this.text = text;
    }

    public Tweet() {

    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
