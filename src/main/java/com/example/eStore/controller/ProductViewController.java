package com.example.eStore.controller;

import com.example.eStore.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("product")
@Controller
public class ProductViewController extends BaseViewController{
    @Autowired
    private ProductService productService;

    @RequestMapping("/list")
    public String list(HttpServletRequest request, Model model) {
        model.addAttribute("products", this.productService.getAll());
        model.addAttribute("editLink", "/product/edit/");
        loadHeaderData(model);

        return "product/list";
    }

    @RequestMapping("/add")
    public String add(HttpServletRequest request, Model model) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("product", null);
        model.addAttribute("action", "Add");
        loadHeaderData(model);

        return "product/add";
    }

    @RequestMapping("/edit/{id}")
    public String edit(HttpServletRequest request, Model model, @PathVariable("id") String id) {
        model.addAttribute("userId", this.utilityService.getCurrentUserId());
        model.addAttribute("product", this.productService.getProductBy(Integer.parseInt(id)).orElse(null));
        model.addAttribute("action", "Edit");
        loadHeaderData(model);

        return "product/add";
    }
}